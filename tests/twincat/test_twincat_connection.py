"""
Test the connection extension
"""

from twinpy.twincat.connection import TwincatConnection

tc = TwincatConnection()

print(tc.is_open)

print(tc.get_module_info('Controller'))
