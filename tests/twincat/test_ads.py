import pyads

# connect to plc and open connection
plc = pyads.Connection('127.0.0.1.1.1', 350)
plc.open()

# read int value by name
counter = plc.read_by_name('GVL.counter', pyads.PLCTYPE_UINT)

print(counter)

# write int value by name
# plc.write_by_name('GVL.int_val', i, pyads.PLCTYPE_INT)

# close connection
plc.close()
