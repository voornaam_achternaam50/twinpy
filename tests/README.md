This directory contains quick tests and can always be safely deleted.

These quick tests were used in development. Automated tests could have been applied here, but the required integration with Simulink/TwinCAT makes that difficult.
