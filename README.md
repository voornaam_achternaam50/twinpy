Edit!

# TwinPy

[![CodeFactor](https://www.codefactor.io/repository/bitbucket/ctw-bw/twinpy/badge?s=a7ae9b43e3ce8764663908b3985134f45c98052a)](https://www.codefactor.io/repository/bitbucket/ctw-bw/twinpy)
[![Documentation Status](https://readthedocs.org/projects/twinpy/badge/?version=latest)](https://twinpy.readthedocs.io/en/latest/?badge=latest)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

This repository contains a GUI made in Python to interface with Simulink models in TwinCAT.

It has two main functions: maps to Simulink models can be created, and linked to TwinCAT, and GUI widgets that read and write from those.

Interfacing with TwinCAT is done through ADS, the variable sharing service.

API documentation can be found here: https://twinpy.readthedocs.io/en/latest/

## Install package

After cloning this repository, run:

```
pip install -e ./twinpy
```

To install the package from local source. The `-e` flag keeps the original source editable, which is convenient during development.

It is recommended to install a local package like twinpy only in a virtual environment. 

## Getting started

 1. To run from a virtual environment (recommended), open a terminal in this directory and run: `python -m venv venv` (use a full path to select a different Python version). A virtual environment is now created.
 2. Run `venv/Scripts/Activate.ps1` for PowerShell or `venv/Scripts/activate.bat` for CMD. Your terminal should now show a '(venv)' prefix, the environment has been activated.
 4. Install the regular packages with `python -m pip install -r requirements.txt`
 5. You can now run `python -m twinpy`

In case you do not want to use a virtual environment, just skip steps 1 and 2.

## Run with active terminal

By default `app.exec()` is a hanging command that fires up the main event loop of the GUI. It will prevent the script from exiting when the application is still open. When running the GUI from a terminal, you cannot interact with the active objects untill the GUI was closed.

Pyzo has a way around this. If you run the script from a shell with 'gui' set to "PyQt5" you can disable the `app.exec()` line. The interpreter will handle the QT event loop elsewhere, freeing up the Python console for you to use.

## TwinCAT Interface

Interfacing with TwinCAT starts with the SimulinkModel object. It points to a Simulink object running in TwinCAT. You can address parts of the model in a hierarchical way, based on the hierarchy in the original Simulink model:

```
model = SimulinkModel(...)
const_block = model.MySubSystem.OtherSubSystem.MyConstBlock
```

Parameters are listed as properties of blocks. Use `get()` and `set()` to read and write those parameters:

```
val = model.MySubSystem.OtherSubSystem.MyConstBlock.Value.get()
# A Constant block only has one parameter, named `Value`
```

If a block only has a single parameter or signal, you can also use `get()` and `set()` directly on the block:

```
val = model.MySubSystem.OtherSubSystem.MyConstBlock.get()  # Same result as above
```

Signals are named `si{n}` and `so{n}` for input and output signals, numbered in the order determined by Simulink. Parameters are named after their Simulink name.

## Custom GUI

To customize the GUI for your application, you can extend the `BaseGUI` class with your own. To maintain clean version control, create your own repository and load this base repo as a git submodule.

## Docs ##

Documentation from the source can be compiled using sphinx:

```
cd docs
make html
```

Note: if you get an error about e.g. missing the pyads package because you use a virtual environment, install sphinx inside the virtual environment too. 

Documentation is automatically built and hosted on readthedocs.io: 
