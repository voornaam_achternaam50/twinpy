Modules
=======

TwinCAT and Simulink Interfacing
--------------------------------

TwinCAT Connection
~~~~~~~~~~~~~~~~~~

.. automodule:: twinpy.twincat.connection
    :members:
    :show-inheritance:
    :undoc-members:

Simulink
~~~~~~~~

.. automodule:: twinpy.twincat.simulink
    :members:
    :show-inheritance:
    :undoc-members:

ADS Variables
~~~~~~~~~~~~~

.. automodule:: twinpy.twincat.symbols
    :members:
    :show-inheritance:
    :undoc-members:


GUI
---

TwinCAT UI Elements
~~~~~~~~~~~~~~~~~~~

.. automodule:: twinpy.ui.tc_widgets
    :members:
    :show-inheritance:
    :undoc-members:


Base Widgets
~~~~~~~~~~~~

.. automodule:: twinpy.ui.base_widgets
    :members:
    :show-inheritance:
    :undoc-members:


Base GUI
~~~~~~~~

.. automodule:: twinpy.ui.base_gui
    :members:
    :show-inheritance:
    :undoc-members:


Main - Example
--------------

.. automodule:: twinpy.__main__


Module Info
-----------

.. automodule:: twinpy
    :members:
    :show-inheritance:
    :undoc-members:
