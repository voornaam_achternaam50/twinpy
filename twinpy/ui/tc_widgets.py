"""TwinCAT widgets are Qt elements that are easily linked to an ADS symbol

E.g. a label that shows an output value or an input box which changes a
parameter.

The `@pyqtSlot()` is Qt decorator. In many cases it is not essential, but
it's good practice to add it anyway.
"""

from typing import Optional, Any, Tuple, Union, Callable
from abc import ABC, ABCMeta, abstractmethod

from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QFrame
from PyQt5.QtCore import pyqtSlot

from ..twincat.symbols import Symbol


class QABCMeta(ABCMeta, type(QWidget)):
    """Create a meta class that combines ABC and the Qt meta class"""


class Color:  # pylint: disable=too-few-public-methods
    """Collection of useful colours"""

    DEFAULT = "#FFFFFF"  # White
    EDITING = "#FFFFAA"  # Yellow-ish


class TcWidget(ABC, metaclass=QABCMeta):
    """Abstract class, to be multi-inherited together with a Qt item"""

    EVENT_NOTIFICATION = "notification"
    EVENT_NONE = "none"

    def __init__(self, *args, **kwargs):
        """Constructor

        It is important to call this init() as late as possible from a
        subclass! The order should be:

           1. Subclass specific stuff (e.g. number formatting)
           2. Call to super().__init__(...) - Now the QWidget stuff has been
              made ready
           3. QWidget related stuff

        :param args:
        :param kwargs: See list below - kwargs are passed along to
            `connect_symbol` too

        :kwargs:
            * `symbol`: twincat.symbols.Symbol to link to (i.e. to read from
                        and/or write to)
            * `format`: Formatter symbol, e.g. '%.1f' or '%d' or callable
                        function with a single argument ('%.3f' by default) (ignored
                        when not relevant)

        """

        self._symbol: Optional[Symbol] = kwargs.pop("symbol", None)
        # Included None fallback

        self.value_format: Union[str, Callable] = kwargs.pop("format", "%.3f")

        self.event_type = self.EVENT_NOTIFICATION
        # Already set default event type

        self._handles: Optional[Tuple[int, int]] = None
        # Handles to the notification specific for this widget

        if self._symbol:
            self.connect_symbol(self._symbol, *args, **kwargs)
            # Connect already if passed

    def connect_symbol(self, new_symbol: Optional[Symbol] = None, **kwargs):
        """Connect a symbol (copy is left as property)

        By default a device callback is created with an on-change event
        from TwinCAT.
        Old callbacks are deleted first. Pass None to only clear callbacks.
        The notification handles are stored locally.
        Extend (= override but call the parent first) this method to
        configure more of the widget, useful if e.g. widget callbacks depend
        on the symbol.

        :param new_symbol: Symbol to link to (set None to only clear the
            previous)
        :param kwargs: See list below - Keyword arguments are passed along as
            device notification settings too

        kwargs:
            `event_type`
        """

        if self._handles is not None and self._symbol is not None:
            self._symbol.del_device_notification(self._handles)
            # In case previous callback existed, clear it
            self._handles = None

        self._symbol = new_symbol

        if "event_type" in kwargs:
            self.event_type = kwargs.pop("event_type")

        if new_symbol is not None:
            if self.event_type == self.EVENT_NOTIFICATION:
                self._handles = self._symbol.add_device_notification(
                    self.twincat_receive, **kwargs
                )

            # It seems a notification is always fired on creation, so we don't
            # need to call it now
            # self.twincat_receive(self._symbol.get())

    @abstractmethod
    def twincat_receive(self, value):
        """Callback attached to the TwinCAT symbol

        :param value: New remote value
        """

    def twincat_send(self, value: Any):
        """Set value in symbol (and send to TwinCAT)"""
        if self._symbol is not None:  # Save for unconnected symbols
            self._symbol.set(value)

    def format(self, value: Any) -> str:
        """"Use the stored formatting to created a formatted text"""
        if isinstance(self.value_format, str):
            return self.value_format % value
        if callable(self.value_format):
            return self.value_format(value)
        raise NotImplementedError(
            "The format `{}` could not be " "processed".format(self.value_format)
        )

    def __del__(self):
        """Destructor"""

        if self._symbol:
            # Element is about to become extinct, so clear callbacks
            self._symbol.clear_device_notifications()


class TcLabel(QLabel, TcWidget):
    """Label that shows a value"""

    def __init__(self, *args, **kwargs):
        """Constructor

        :param args:
        :param kwargs: See list below
        """

        super().__init__(*args, **kwargs)  # Both constructors will be called

        # Prevent it being empty from the start
        if self._symbol is None and not self.text():
            self.setText("NaN")  # Default value

        # Give the label a frame to visually indicate it is not static
        self.setFrameStyle(QFrame.Panel | QFrame.Sunken)

    def twincat_receive(self, value):
        self.setText(self.format(value))


class TcLineEdit(QLineEdit, TcWidget):
    """Readable and writable input box"""

    def __init__(self, *args, **kwargs):
        """Constructor

        :param args:
        :param kwargs: See list below
        """

        super().__init__(*args, **kwargs)  # Both constructors will be called

        self.textEdited.connect(self.on_text_edited)
        self.editingFinished.connect(self.on_editing_finished)

    def twincat_receive(self, value) -> Any:
        self.setText(self.format(value))

    @pyqtSlot()
    def on_editing_finished(self):
        """Called when [Enter] is pressed or box loses focus"""

        self.setStyleSheet("background-color:" + Color.DEFAULT)

        value = self.text()
        self.twincat_send(value)

    @pyqtSlot(str)
    def on_text_edited(self, *_value):
        """Callback when text was modified (i.e. on key press)"""
        self.setStyleSheet("background-color:" + Color.EDITING)


class TcPushButton(QPushButton, TcWidget):
    """Button that sends value when button is held pressed"""

    def __init__(self, *args, **kwargs):
        """Constructor

        :param args:
        :param kwargs:

        :kwargs:
            * `value_pressed`: Value on press (default: 1), None for no action
            * `value_released`: Value on release (default: 0), None for no action
        """

        self.value_pressed = kwargs.pop("value_pressed", 1)
        self.value_released = kwargs.pop("value_released", 0)

        super().__init__(*args, **kwargs)

    def connect_symbol(self, new_symbol: Optional[Symbol] = None, **kwargs):
        """Extend symbol connection"""

        super().connect_symbol(new_symbol, **kwargs)  # Call parent

        if self.value_pressed is not None:
            self.pressed.connect(self.on_pressed)
        if self.value_released is not None:
            self.released.connect(self.on_released)

    @pyqtSlot()
    def on_pressed(self):
        """Callback on pressing button"""
        self._symbol.set(self.value_pressed)

    @pyqtSlot()
    def on_released(self):
        """Callback on releasing button"""
        self._symbol.set(self.value_released)

    def twincat_receive(self, value):
        """Do nothing, method requires definition anyway"""
