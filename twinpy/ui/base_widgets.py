"""These widgets are specific implementations of TcWidgets

They are not intended to be overridden again. They are separate classes mostly
because their specific logic became significant.
"""

from typing import List

from PyQt5.QtCore import Qt

from .tc_widgets import TcLabel


class TcErrorsLabel(TcLabel):
    """Extension of TcLabel for a list of joint errors

    This is separate class because the amount of logic got a little bit much.
    """

    def __init__(self, *args, **kwargs):

        # Use local format function (neatly attached to format callback)
        if "format" not in kwargs:
            kwargs["format"] = self.format_errors_list

        super().__init__(*args, **kwargs)

        self.setTextFormat(Qt.RichText)  # Allow HTML-like tags

    @staticmethod
    def format_errors_list(error_list: List[int]) -> str:
        """Set text for errors label"""

        text = ""

        for error in error_list:
            if text:
                text += "<br>"  # In rich text, use HTML break instead of \n

            code = TcErrorsLabel.to_hex(error)

            if error > 0:
                line = "<font color=red><b>" + code + "</b></font>"
            else:
                line = "<font color=grey>" + code + "</font>"

            text += line

        return text

    @staticmethod
    def to_hex(value: int) -> str:
        """Create human-readable hex from integer"""

        code = hex(abs(value)).upper()[2:].rjust(8, "0")

        code = code[0:4] + " " + code[4:]  # Add a space for readability

        return code
