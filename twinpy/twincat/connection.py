"""Extension on pyads TwinCAT connection"""


import pyads


class TwincatConnection(pyads.Connection):
    """Extend default Connection object (typically named `plc`)

    ADS connection with custom features.
    """

    def __init__(
        self,
        ams_net_id: str = "127.0.0.1.1.1",
        ams_net_port: int = 350,
        ip_address: str = None,
    ):
        """Constructor

        Note that this version will connect on object creation, throwing an exception
        when it fails. `pyads.Connection` waits for `.open()` and will fail quietly.

        :param ams_net_id: TwinCAT AMS address (default is localhost)
        :param ams_net_port: ADS Port (default is 350)
        :param ip_address: Target IP (automatically deduced from AMS address)
        :raises pyads.ADSError: When connection failed
        """

        super().__init__(ams_net_id, ams_net_port, ip_address)

        self.open()

        if not self.is_open:
            raise pyads.ADSError(
                text="Connection to TwinCAT could not be "
                "established. Is TwinCAT in run mode?"
                "Are the port and address correct?"
            )

    def get_module_info(self, module_name: str) -> dict:
        """Get information about live module"""

        # Read all info as list of bytes (doing it as a list of variable
        # names fails)
        try:
            var_name = module_name + ".ModuleInfo"
            data = self.read_by_name(var_name, pyads.PLCTYPE_ARR_DINT(37))
        except pyads.ADSError as err:
            # Re-raise error
            raise pyads.ADSError(
                text="Could not connect to TwinCAT module. "
                "Does the target module exist in the "
                "running TwinCAT instance?"
            ) from err

        return {
            "ClassId": data[0:4],
            "BuildTimeStamp": data[4],
            "ModelCheckSum": data[5:9],
            "ModelVersion": data[9:13],
            "TwinCatVersion": data[13:17],
            "TcTargetVersion": data[17:21],
            "MatlabVersion": data[21:25],
            "SimulinkVersion": data[25:29],
            "CoderVersion": data[29:33],
            "TcTargetLicenseID": data[33:38],
        }
